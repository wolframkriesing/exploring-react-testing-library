# exploring-react-testing-library

## Run it

- `docker-compose up --detach` will build the docker env on the first run, later it will just start it in the background
- `docker exec -it node bash` enter the docker container running node (so we all have the same env)
- now you should have a shell inside the docker container, it should look something like this:
  `root@b7f73b4c:/app#` in there do
  - `yarn install` setup the node env
  - `yarn test` to run the tests