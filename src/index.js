import * as React from 'react';
import {describe, it} from 'mocha';
import {cleanup, render, act, fireEvent} from '@testing-library/react';
import jsdom from 'mocha-jsdom';

globalThis.document = jsdom({
  url: "http://localhost:3000"
})

const slowFn = () => new Promise((resolve) => setTimeout(resolve, 100));

const Comp = () => {
  const [s, setS] = React.useState(false);
  const [s1, setS1] = React.useState(false);
  React.useEffect(() => {
    const asyncFn = async () => {
      await slowFn();
      setS1(true);
    };
    asyncFn(); // this calls an async fn without awaiting the result :(
  }, [s])
  const onClick = () => {setS(true)};
  return React.createElement(
    'button',
    {onClick, children: 'do something async'}
  );
}

afterEach(cleanup);

describe('Trigger a sequence of async actions', () => {
  it('WHEN clicking THEN how do I make this test run????', () => {
    const renderedResult = render(React.createElement(Comp));

    act(() => {
        fireEvent.click(renderedResult.getByText('do something async'));
    });
  });
});